Brandwatch Christmas Puzzler 2021
=================================

This is my submission for the 2021 Brandwatch Christmas Puzzler.

The Christmas Puzzler this year is to shuffle the words of an arbitrary amount of text.
A word is anything made of letters but not numbers or punctuation.

There is a second puzzle which is to reconstruct the original text from the shuffled text.

I like to tackle these problems using unusual techniques.

Word Shuffler
-------------

I have decided to implement as much of the word shuffler as possible using quantum computing.
Detailing the exploration of the Qiskit framework and the derivation of the algorithm is too much for this readme.
If you are interested then they have been written up in the following blog posts:

 * [Quantum Computing: The Qiskit Framework](https://blog.franglen.io/2021/12/26/quantum-computing-qiskit.html):
   an exploration of the Qiskit framework and the construction of some simple logic gates
 * [Quantum Computing: Understanding Quantum Circuits](https://blog.franglen.io/2021/12/30/quantum-circuits-intuitition.html):
   an attempt to understand quantum computing, coming from the context of a classical programmer
 * [Quantum Computing: Creating a full Algorithm](https://blog.franglen.io/2022/01/01/quantum-shuffler.html):
   the implementation of the quantum shuffling algorithm with performance testing

To run this you need python 3.9 and poetry.
You can then run this using `make shuffle`.
This will shuffle the first chapter of Moby Dick, found in the `data/moby-dick.txt` file (4,493 tokens in 6m 8s).

If you wish to review the source code then look in the `src/shuffle/` folder.
The entrypoint is the `__main__.py` file, which uses `tokenizer.py` to split the text into words and `index_shuffler.py` to shuffle them.

Word Unshuffler
---------------

Not implemented.

Poetry Installation and Usage
-----------------------------

This uses [poetry](https://poetry.eustace.io/docs/) as the package manager.
You can install it by following the instructions [here](https://poetry.eustace.io/docs/#installation).
I recommend using [pipx](https://pipxproject.github.io/pipx/) to install it, which requires installing pipx:

```
pip install --user pipx
pipx install poetry
```

Poetry uses automatically managed virtual environments.
This template already has a pyproject.toml file so to create such an environment and install the dependencies you just need to run:
```
make requirements
```

You can install new packages with the command:
```
poetry install DEPENDENCY
```
This will record the changes in the pyproject.toml and poetry.lock files, so remember to commit them.

You can run commands directly in the virtual environment without activating it.
This is done by adding the `poetry run` prefix to your command, for example:
```
poetry run python -c 'print("Hello, World!")'
```
Invoking commands in this way means you will not change the virtual environment accidentally.

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
<p><small>Refined to use poetry as the package manager</small></p>
