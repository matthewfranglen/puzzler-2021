from pathlib import Path

import typer
from tqdm.auto import tqdm

from src.shuffle.index_shuffler import shuffle
from src.shuffle.tokenizer import tokenize


def main(
    source: Path = typer.Option(..., help="Text file to shuffle"),
    destination: Path = typer.Option(..., help="File to write shuffled text to"),
) -> None:
    source_text = source.read_text(encoding="utf-8")
    tokenized_text = list(tokenize(source_text))
    shuffled_text = "".join(
        shuffle(word) if word.isalpha() else word for word in tqdm(tokenized_text)
    )
    destination.write_text(shuffled_text, encoding="utf-8")


if __name__ == "__main__":
    typer.run(main)
