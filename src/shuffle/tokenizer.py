from typing import Iterator


def tokenize(text: str) -> Iterator[str]:
    """
    This tokenizes the text into sequences of letters and sequences of non letters.
    """

    isalpha = False
    batch = []
    for letter in text:
        if letter.isalpha():
            if not isalpha:
                if batch:
                    yield "".join(batch)
                    batch = []
                isalpha = True
        else:
            if isalpha:
                if batch:
                    yield "".join(batch)
                    batch = []
                isalpha = False
        batch.append(letter)
    if batch:
        yield "".join(batch)
