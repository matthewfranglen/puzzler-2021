import math

from qiskit import QuantumCircuit
from qiskit.result.result import Result


def run(qc: QuantumCircuit, shots: int = 1024) -> Result:
    """
    This will execute the quantum circuit provided,
    returning the result wrapper.
    The shots parameter indicates the number of times to run the circuit.
    """
    # This import is here because execute is a super generic term
    from qiskit import Aer, execute  # pylint: disable=import-outside-toplevel

    # The qasm_simulator does not allow inspection of qubit states except through measure
    backend = Aer.get_backend("qasm_simulator")
    job = execute(qc, backend, shots=shots, memory=True)
    return job.result()


def to_rotation(probability: float) -> float:
    """
    This converts the probability into a rotation from |0> which will put the
    qubit into a superposition that has probability chance of being in |1>
    state.

    This is discussed in
    https://blog.franglen.io/2021/12/30/quantum-circuits-intuitition.html#Rotation-Gate
    """
    return math.acos(-(probability * 2 - 1))


def initialize(qc: QuantumCircuit, value: int, offset: int) -> None:
    """
    This copies the 8 bit int into the quantum circuit qubits at the specified offset.
    """
    binary_value = bin(value)[2:][::-1]
    assert len(binary_value) <= 8
    for index, bit in enumerate(binary_value, start=offset):
        if bit == "1":
            qc.x(index)


def conditional_swap(
    qc: QuantumCircuit, control_qubit: int, left_offset: int, right_offset: int
) -> None:
    """
    This swaps two pairs of 8 qubits if the control_qubit is in the |1> state
    """
    for left_qubit, right_qubit in zip(
        range(left_offset, left_offset + 8), range(right_offset, right_offset + 8)
    ):
        qc.cswap(
            control_qubit=control_qubit,
            target_qubit1=left_qubit,
            target_qubit2=right_qubit,
        )
