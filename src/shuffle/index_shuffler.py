"""
This is the stepwise shuffling algorithm that is discussed in
https://blog.franglen.io/2022/01/01/quantum-shuffler.html#Optimizing-for-the-Simulator

This runs a quantum circuit once for each letter swap that occurs.

This is performant enough to be used for the puzzler.
"""
from qiskit import QuantumCircuit

from src.shuffle.base import run, to_rotation


def shuffle(text: str) -> str:
    """
    This will shuffle the text provided.
    """
    if not text:
        return ""

    values = list(text)
    shuffled = []
    while len(values) > 1:
        index = choose(len(values))
        shuffled.append(values.pop(index))
    shuffled.append(values[0])

    return "".join(shuffled)


def choose(n: int) -> int:
    """
    This creates and runs a quantum circuit that will
    select one value between 0 and n-1 inclusive.
    """
    qc = _make_chooser_circuit(n)
    output = run(qc, shots=1).get_memory()[0]
    return output.index("1")


def _make_chooser_circuit(n: int, barriers: bool = False) -> QuantumCircuit:
    """
    This creates a quantum circuit that will
    create n qubits
    and set one qubit to |1>
    and all other qubits to |0>.
    """
    qc = QuantumCircuit(n, n)

    # This calculates the rotation required to produce the superposition with
    # the correct outcome distribution
    # The input to to_rotation is the desired probability distribution
    # For example, given n = 3:
    #   {
    #       0: to_rotation(1 / (3 - 0)), # 1 / 3
    #       1: to_rotation(1 / (3 - 1)), # 1 / 2
    #       2: to_rotation(1 / (3 - 2)), # 1 / 1
    #   }
    rotations = {v: to_rotation(1 / (n - v)) for v in range(n)}

    # This puts the control qubits into superpositions that match their probability distribution
    for qubit, rotation in rotations.items():
        qc.rx(theta=rotation, qubit=qubit)

    # Barriers can make the visualization of the circuit easier to follow.
    # You can construct the circuit and visualize it in jupyter using:
    #   qc = _make_chooser_circuit(3, barriers=True)
    #   display(qc.draw(output="mpl"))
    # You do not need to run the circuit to visualize it.
    if barriers:
        qc.barrier()

    # This entangles the control qubits such that
    # the first control qubit that is in state |1>
    # is the only qubit that is in state |1>
    for control_qubit in range(n):
        for target_qubit in range(control_qubit + 1, n):
            target_rotation = rotations[target_qubit]
            # This rotates the target_qubit back to |0> if the control qubit is |1>
            qc.crx(
                theta=-target_rotation,
                control_qubit=control_qubit,
                target_qubit=target_qubit,
            )
        if barriers:
            qc.barrier()

    # This copies the qubits into classical bits
    qc.measure(range(n), range(n))

    return qc
