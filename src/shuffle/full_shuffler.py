"""
This is the full shuffling algorithm that is discussed in
https://blog.franglen.io/2022/01/01/quantum-shuffler.html

This becomes extremely inefficient at 6 entries or above and so is not used for
the puzzler.
"""

from typing import List

from qiskit import QuantumCircuit

from src.shuffle.base import conditional_swap, initialize, run, to_rotation


def shuffle(values: List[int]) -> List[int]:
    """
    This takes the list of values and shuffles them, returning the shuffled list.
    If you wish to use this on a string then you should convert it to ints first.
    This would only work on single byte letters as it assumes that each int is
    mappable to a byte and can be freely reordered.

        shuffled_letters = shuffle(
            [ord(letter) for letter in "hello"]
        )
        shuffled_word = "".join(
            chr(value) for value in shuffled_letters
        )
    """
    qc = _make_shuffler_circuit(values)
    output = run(qc, shots=1).get_memory()[0]
    return [
        int(output[index * 8 : (index + 1) * 8], base=2)
        for index in range(len(output) // 8)
    ][::-1]


def _make_shuffler_circuit(values: List[int], barriers: bool = False) -> QuantumCircuit:
    """
    This constructs the quantum circuit to produce the shuffled letters.
    """
    value_count = len(values)

    # The control qubits are grouped per value to be chosen.
    # One qubit represents each value that could replace the value at index 0.
    # For example, if we have the values [1, 2, 3]
    # the first control qubit group would have two values,
    #   one representing putting index 1 (value 2) at index 0
    #   one representing putting index 2 (value 3) at index 0
    #   no value represents putting index 0 (value 1) at index 0
    #       instead this happens if no other swap occurs
    # the second control qubit group would have one value,
    #   one representing putting index 2 (value 3) at index 1
    #   no value represents putting index 1 (value 2) at index 1
    #       for the same reason as before
    control_qubits = sum(range(1, value_count))

    # The two values are the number of qubits to use and the number of classical bits.
    # The output of the circuit is measured and the measurements copy qubits to classical bits.
    # Since the output is the shuffled list we do not need to copy the control qubits.
    qc = QuantumCircuit(
        (8 * value_count) + control_qubits,
        8 * value_count,
    )

    # This copies the list values into the circuit
    for index, value in enumerate(values):
        initialize(qc, value=value, offset=index * 8)

    # Barriers can make the visualization of the circuit easier to follow.
    # You can construct the circuit and visualize it in jupyter using:
    #   qc = _make_shuffler_circuit([1, 2, 3], barriers=True)
    #   display(qc.draw(output="mpl"))
    # You do not need to run the circuit to visualize it.
    if barriers:
        qc.barrier()

    # This operates at the level of the control qubit group,
    # selecting an appropriate swap to perform.
    # It is done one less time than there are values.
    # For example, if we have the values [1, 2, 3]
    #   The first swap would be for index 0 and would consider values [1, 2, 3]
    #   The second swap would be for index 1 and would consider values [2, 3]
    #   The third swap is not needed as it would only consider a single value

    # This is the position of the start of the control qubit group
    control_qubit_offset = len(values) * 8

    for iteration in range(value_count - 1):
        # This is the number of values being considered for the swap
        count = value_count - iteration
        _make_shuffle_step(
            qc=qc,
            n=count,
            value_qubit_offset=8 * iteration,
            control_qubit_offset=control_qubit_offset,
        )
        control_qubit_offset += count - 1
        if barriers:
            qc.barrier()

    # This copies the qubits into classical bits
    qc.measure(range(8 * value_count), range(8 * value_count))

    return qc


def _make_shuffle_step(
    qc: QuantumCircuit,
    n: int,
    value_qubit_offset: int,
    control_qubit_offset: int,
) -> None:
    """
    This performs a single swap.
    The swap will write to the 8 qubits starting at value_qubit_offset.
    n - 1 control qubits are used starting from control_qubit_offset.
    """
    # This puts the control qubits into a state where
    # up to one control qubit is |1> (true)
    # and the other control qubits are |0> (false)
    # The specific one that is true is not determined as no measurement has been made
    _nway_chooser(qc=qc, n=n, control_qubit_offset=control_qubit_offset)

    # This entangles the target value with all of the potential values it could be swapped with,
    # using the control qubits.
    # When measured one of the swaps will occur.
    for index in range(n - 1):
        conditional_swap(
            qc,
            control_qubit=control_qubit_offset + index,
            left_offset=value_qubit_offset,
            right_offset=value_qubit_offset + (8 * (index + 1)),
        )


def _nway_chooser(
    qc: QuantumCircuit,
    n: int,
    control_qubit_offset: int,
) -> None:
    """
    This creates a superposition where
    up to one control qubit is |1> (true)
    and the other control qubits are |0> (false)
    """
    # This algorithm is discussed in
    # https://blog.franglen.io/2021/12/30/quantum-circuits-intuitition.html#N-Bit-Chooser
    # In this implementation one choice is excluded as it can be inferred from the others

    # n less one because not doing something is a choice
    n = n - 1

    # This calculates the rotation required to produce the superposition with
    # the correct outcome distribution
    # The input to to_rotation is the desired probability distribution
    # For example, given n = 3:
    #   {
    #       0: to_rotation(1 / (1 + 3 - 0)), # 1 / 4
    #       1: to_rotation(1 / (1 + 3 - 1)), # 1 / 3
    #       2: to_rotation(1 / (1 + 3 - 2)), # 1 / 2
    #   }
    rotations = {v: to_rotation(1 / (1 + n - v)) for v in range(n)}

    # This puts the control qubits into superpositions that match their probability distribution
    for qubit, rotation in rotations.items():
        qc.rx(theta=rotation, qubit=control_qubit_offset + qubit)

    # This entangles the control qubits such that
    # the first control qubit that is in state |1>
    # is the only qubit that is in state |1>
    for control_index in range(n - 1):
        for target_index in range(control_index + 1, n):
            control_qubit = control_qubit_offset + control_index
            target_qubit = control_qubit_offset + target_index
            target_rotation = rotations[target_index]
            # This rotates the target_qubit back to |0> if the control qubit is |1>
            qc.crx(
                theta=-target_rotation,
                control_qubit=control_qubit,
                target_qubit=target_qubit,
            )
